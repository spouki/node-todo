var express = require('express');
var router = express.Router();

/* GET tasks listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/my', function(req, res, next) {
  res.send('My tasks');
});

router.get('/shared', function(req, res, next) {
  res.send('Shared with me');
});

router.post('/new', function(req, res, next) {
  res.send('Posting new task');
});

router.put('/:id', function(req, res, next) {
  var send = "Modification of task " + req.params.id;
  res.send(send);
});

router.delete('/:id', function(req, res, next) {
  var send = "Deletion of task " + req.params.id;
  res.send(send);
});

router.post('/share/:id', function(req, res, next) {
  var send = "Shared task " + req.params.id + " with someone";
  res.send(send);
});


module.exports = router;
