var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  var mysql      = require('mysql');
  var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'todo'
  });

  connection.connect();

  connection.query('SELECT * from users', function(err, rows, fields) {
    if (err) throw err;
    // console.log('The solution is: ', rows);
    res.send(rows);
  });

  connection.end();
});


router.post('/auth', function(req, res, next) {
  res.send('Authentificated');
});

router.post('/newuser', function(req, res, next) {
  res.send('New user created');
});

router.post('/', function(req, res, next) {
  res.send('Modified user profile');
});

module.exports = router;
